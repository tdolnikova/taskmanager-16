package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.api.deltaspike.ProjectRepository;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import lombok.NoArgsConstructor;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.Nullable;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Collection;
import java.util.logging.Logger;

@Transactional
@ApplicationScoped
@NoArgsConstructor
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @Inject
    private ProjectRepository projectRepository;

    private final Logger LOGGER = Logger.getLogger(ProjectService.class.getName());

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        LOGGER.info("[Поиск проекта по id]");
        System.out.println("user_id: " + userId + "\nid: " + id);
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findProjectByIdAndUserId(id, userId);
    }

    @Override
    public Project findOneByName(@Nullable final String name) {
        LOGGER.info("[Поиск проекта по названию [ " + name + " ]");
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findProjectByName(name);
    }

    @Override
    public @Nullable Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public @Nullable Collection<Project> findAll(@Nullable final User user) {
        LOGGER.info("[Поиск всех проектов]");
        if (user == null) return null;
        Collection<Project> c = projectRepository.findAllByUserId(user.getId());
        System.out.println("- - -");
        for (Project p : c) System.out.println(p.getName());
        System.out.println("- - -");
        return projectRepository.findAllByUserId(user.getId());
    }

    @Override
    public @Nullable Collection<Project> findAllByName(@Nullable final String userId, @Nullable final String text) {
        LOGGER.info("[Поиск всех проектов по названию]");
        if (userId == null || userId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return projectRepository.findAllByUserIdAndNameContaining(userId, text);
    }

    @Override
    public @Nullable Collection<Project> findAllByDescription(@Nullable final String userId, @Nullable final String text) {
        LOGGER.info("[Поиск всех проектов по описанию]");
        if (userId == null || userId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return projectRepository.findAllByUserIdAndDescriptionContaining(userId, text);
    }

    @Override
    public void persist(@Nullable final Project project) {
        LOGGER.info("[Вставка проекта]");
        if (project == null) return;
        projectRepository.save(project);
    }

    @Override
    public void persistList(@Nullable Collection<Project> list) {
        if (list == null || list.isEmpty()) return;
        for (Project project : list) {
            persist(project);
        }
    }

    @Override
    public void merge(@Nullable final String newData,
                      @Nullable final Project project,
                      @Nullable final DataType dataType) {
        LOGGER.info("[Изменение данных проекта]");
        if (newData == null || newData.isEmpty()) return;
        if (project == null || dataType == null) return;
        if (dataType.equals(DataType.NAME)) {
            project.setName(newData);
            projectRepository.save(project);
        }
        if (dataType.equals(DataType.DESCRIPTION)) {
            project.setDescription(newData);
            projectRepository.save(project);
        }
    }

    @Override
    public void remove(@Nullable final Project project) {
        LOGGER.info("[Удаление проекта]");
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        LOGGER.info("[Удаление всех проектов пользователя]");
        if (userId == null || userId.isEmpty()) return;
        projectRepository.deleteAllByUserId(userId);
    }

}
