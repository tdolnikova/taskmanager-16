package com.dolnikova.tm.endpoint;

import com.dolnikova.tm.dto.SessionDTO;
import com.dolnikova.tm.dto.UserDTO;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.util.DtoConversionUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@WebService
@NoArgsConstructor
public class UserEndpoint extends AbstractEndpoint {

    @WebMethod
    @Nullable
    public UserDTO findOneByLoginUser(@WebParam(name = "name") @Nullable String login) {
        return DtoConversionUtil.userToDto(userService.findOneByLogin(login));
    }

    @WebMethod
    public @Nullable UserDTO findOneBySession(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        return DtoConversionUtil.userToDto(userService.findOneBySession(session));
    }

    @WebMethod
    public @Nullable List<UserDTO> findAllUser(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        @Nullable final User currentUser = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, currentUser);
        validate(session);
        if (session == null) return null;
        @Nullable final Collection<User> users = userService.findAll();
        if (users == null) return null;
        @Nullable final List<UserDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            userDTOs.add(DtoConversionUtil.userToDto(user));
        }
        return userDTOs;
    }

    @WebMethod
    public @Nullable List<UserDTO> findAllByLoginUser(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
                                                      @WebParam(name = "text") @Nullable String text) {

        if (sessionDTO == null) return null;
        @Nullable final User currentUser = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, currentUser);
        validate(session);
        if (session == null) return null;
        @Nullable final String userId = session.getUser().getId();
        @Nullable final Collection<User> users = userService.findAllByLogin(userId, text);
        if (users == null) return null;
        @Nullable final List<UserDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            userDTOs.add(DtoConversionUtil.userToDto(user));
        }
        return userDTOs;
    }

    @WebMethod
    public void persistUser(@WebParam(name = "userDTO") @Nullable UserDTO userDTO) {
        @Nullable final User user = DtoConversionUtil.dtoToUser(userDTO);
        if (user == null) return;
        user.setId(UUID.randomUUID().toString());
        userService.persist(user);
        sessionService.createSession(user);
    }

    @WebMethod
    public void persistListUser(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
                                @WebParam(name = "users") @Nullable List<UserDTO> list) {
        if (sessionDTO == null || list == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        @Nullable final List<User> userList = new ArrayList<>();
        for (UserDTO userDTO : list) {
            userList.add(DtoConversionUtil.dtoToUser(userDTO));
        }
        userService.persistList(userList);
    }

    @WebMethod
    public void mergeUser(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
                          @WebParam(name = "newData") @Nullable String newData,
                          @WebParam(name = "userDTO") @Nullable UserDTO userDTO,
                          @WebParam(name = "dataType") @Nullable DataType dataType) {
        if (sessionDTO == null) return;
        @Nullable final User user = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        userService.merge(newData, user, dataType);
    }

    @WebMethod
    public void removeUser(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
                           @WebParam(name = "userDTO") @Nullable UserDTO userDTO) {

        if (sessionDTO == null) return;
        @Nullable final User user = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        userService.remove(user);
    }

    @WebMethod
    public void removeAllUser(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO) {
        if (sessionDTO == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        if (session == null) return;
        userService.removeAll();
    }

    @WebMethod
    public boolean checkPassword(
            @WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "userInput") @Nullable String userInput) {
        if (sessionDTO == null) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return false;
        return userService.checkPassword(session.getUser().getId(), userInput);
    }

}
