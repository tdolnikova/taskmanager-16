package com.dolnikova.tm;

import com.dolnikova.tm.bootstrap.Bootstrap;

import javax.enterprise.inject.se.SeContainerInitializer;

public final class Application {

    public static void main(String[] args) {
        SeContainerInitializer.newInstance()
                .addPackages(Application.class).initialize()
                .select(Bootstrap.class).get()
                .init();
    }

}