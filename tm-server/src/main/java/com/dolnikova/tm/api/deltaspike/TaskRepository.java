package com.dolnikova.tm.api.deltaspike;

import com.dolnikova.tm.entity.Task;
import org.apache.deltaspike.data.api.FullEntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

@Repository(forEntity = Task.class)
public interface TaskRepository extends FullEntityRepository<Task, String> {

    @Query("select t from Task t where t.id = ?1 and t.user.id = ?2")
    Task findByIdAndUserId(@Nullable String id, @Nullable String userId);

    @Query("select t from Task t where t.name = ?1 and t.user.id = ?2")
    Task findOneByNameAndUserId(@Nullable String name, @Nullable String userId);

    @Query("select t from Task t where t.user.id = ?1")
    Collection<Task> findAllByUserId(@Nullable String userId);

    @Query("select t from Task t where t.description = ?1 and t.user.id = ?2")
    Collection<Task> findAllByNameAndUserId(@Nullable String name, @Nullable String userId);

    @Query("select t from Task t where t.description = ?1 and t.user.id = ?2")
    Collection<Task> findAllByDescriptionAndUserId(@Nullable String description, @Nullable String userId);

    @Modifying
    @Transactional
    @Query("delete from Task t where t.user.id = ?1")
    void deleteAllByUserId(@Nullable String userId);

}
