package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IUserService extends IService<User> {

    @Nullable
    User findOneById(@Nullable final String id);

    @Nullable
    User findOneByLogin(final @Nullable String name);

    @Nullable
    User findOneBySession(@Nullable final Session session);

    @Nullable Collection<User> findAll();

    @Nullable Collection<User> findAllByLogin(@Nullable String ownerId, @Nullable String login);

    @Override
    void persist(final @Nullable User entity);

    @Override
    void persistList(final @Nullable Collection<User> list);

    void merge(final @Nullable String newData, final @Nullable User entityToMerge, final @Nullable DataType dataType);

    @Override
    void remove(final @Nullable User entity);

    void removeAll();

    boolean checkPassword(final @Nullable String userId, final @Nullable String userInput);
}
