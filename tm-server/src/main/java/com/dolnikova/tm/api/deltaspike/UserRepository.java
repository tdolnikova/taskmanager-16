package com.dolnikova.tm.api.deltaspike;

import com.dolnikova.tm.entity.User;
import org.apache.deltaspike.data.api.FullEntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

@Repository(forEntity = User.class)
public interface UserRepository extends FullEntityRepository<User, String> {

    @Query("select u from User u where u.login = ?1")
    User findUserByLogin(@Nullable String login);

    @Query("select u from User u where u.id = ?1")
    User findUserById(@Nullable String id);

    @Query("select u from User u where u.login = ?1")
    Collection<User> findAllByLogin(@Nullable String login);

}
