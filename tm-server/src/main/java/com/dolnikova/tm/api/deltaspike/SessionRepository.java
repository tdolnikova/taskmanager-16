package com.dolnikova.tm.api.deltaspike;

import com.dolnikova.tm.entity.Session;
import org.apache.deltaspike.data.api.FullEntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

@Repository(forEntity = Session.class)
public interface SessionRepository extends FullEntityRepository<Session, String> {

    @Query("select s from Session s where s.id = ?1")
    Session findSessionById(@Nullable String id);

    @Query("select s from Session s where s.user.id = ?1")
    Session findSessionByUserId(@Nullable String userId);

    @Query("select s from Session s where s.signature = ?1")
    Session findSessionBySignature(@Nullable String signature);

    @Query("select s from Session s where s.user.id = ?1")
    Collection<Session> findAllByUserId(@Nullable String userId);

    @Modifying
    @Transactional
    @Query("delete from Session s where s.user.id = ?1")
    void deleteAllByUserId(@Nullable String userId);

}
