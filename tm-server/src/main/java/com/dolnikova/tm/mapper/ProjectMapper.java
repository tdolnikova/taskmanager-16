package com.dolnikova.tm.mapper;

import com.dolnikova.tm.entity.Project;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ProjectMapper {

    @Nullable
    @Select("SELECT * FROM app_project WHERE user_id = #{userId}")
    @ResultMap("projectResultMap")
    Project findOneByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM app_project WHERE id = #{id} AND user_id = #{userId}")
    @ResultMap("projectResultMap")
    Project findOneByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM app_project WHERE name = #{name}")
    @ResultMap("projectResultMap")
    Project findOneByName(@NotNull @Param("name") String name);

    @Nullable
    @Select("SELECT * FROM app_project WHERE user_id = #{userId}")
    @ResultMap("projectResultMap")
    List<Project> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM app_project WHERE user_id = #{userId} AND name LIKE #{name}")
    @ResultMap("projectResultMap")
    List<Project> findAllByName(@NotNull @Param("userId") String userId, @NotNull String name);

    @Nullable
    @Select("SELECT * FROM app_project WHERE user_id = #{userId} AND description LIKE #{description}")
    @ResultMap("projectResultMap")
    List<Project> findAllByDescription(@NotNull @Param("userId") String userId, @NotNull String description);

    @Insert("INSERT INTO app_project (id, name, user_id) values (#{id}, #{name}, #{userId})")
    void persist(@NotNull @Param("id") String id, @NotNull @Param("name") String name, @NotNull @Param("userId") String userId);

    @Update("UPDATE app_project SET name = #{name} WHERE id = #{id}")
    void updateName(@NotNull @Param("name") String name, @NotNull @Param("id") String id);

    @Update("UPDATE app_project SET description = #{description} WHERE id = #{id}")
    void updateDescription(@NotNull @Param("description") String description, @NotNull @Param("id") String id);

    @Delete("DELETE FROM app_project WHERE id = #{id}")
    void remove(@NotNull @Param("id") String id);

    @Delete("DELETE FROM app_project WHERE user_id = #{userId}")
    void removeAll(@NotNull @Param("userId") String userId);

}
