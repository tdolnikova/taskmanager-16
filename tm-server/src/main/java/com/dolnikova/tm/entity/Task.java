package com.dolnikova.tm.entity;

import com.dolnikova.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_task")
public final class Task extends AbstractEntity implements Serializable {

    private Date dateBegin;

    private Date dateEnd;

    private String name;

    private String description;

    @ManyToOne(targetEntity = User.class)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    @ManyToOne(targetEntity = Project.class)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Project project;

    private Date creationDate = new Date();

    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

}
