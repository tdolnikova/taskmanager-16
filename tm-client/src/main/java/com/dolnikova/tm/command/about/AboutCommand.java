package com.dolnikova.tm.command.about;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.ABOUT;
    }

    @NotNull
    @Override
    public String description() {
        return Command.ABOUT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Task Manager ver.SE-07, " + Manifests.read("buildNumber"));
        System.out.println("Developer: " + Manifests.read("developer"));

    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
