package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.DataType;
import com.dolnikova.tm.endpoint.UserDTO;
import com.dolnikova.tm.endpoint.UserEndpoint;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class UserEditProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_EDIT_PROFILE;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_EDIT_PROFILE_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(AdditionalMessage.ENTER_FIELD_NAME + "\n" + DataType.LOGIN + "\n" + DataType.ROLE);
        @NotNull String fieldToEdit = "";
        while (fieldToEdit.isEmpty()) {
            fieldToEdit = serviceLocator.getScanner().nextLine();
        }
        if (fieldToEdit.equalsIgnoreCase(DataType.LOGIN.toString())) {
            changeData(DataType.LOGIN);
            System.out.println(AdditionalMessage.LOGIN_CHANGED);
        }
        if (fieldToEdit.equalsIgnoreCase(DataType.ROLE.toString())) {
            changeData(DataType.ROLE);
            System.out.println("Role updated");
        }
    }

    private void changeData(@NotNull DataType dataType) {
        System.out.println(AdditionalMessage.ENTER + dataType.toString());
        @NotNull String newData = "";
        while (newData.isEmpty()) {
            newData = serviceLocator.getScanner().nextLine();
        }
        serviceLocator.getUserEndpoint().mergeUser(
                serviceLocator.getSessionDTO(),
                newData,
                serviceLocator.getUserDTO(),
                dataType);
        @Nullable final UserDTO updatedUser = serviceLocator.getUserEndpoint().findOneBySession(serviceLocator.getSessionDTO());
        if (updatedUser != null) {
            System.out.println("Updated user: " + updatedUser.getLogin());
            serviceLocator.setUserDTO(updatedUser);
        }
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }
}
