package com.dolnikova.tm.command.task;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.ProjectDTO;
import com.dolnikova.tm.endpoint.TaskDTO;
import com.dolnikova.tm.endpoint.UserDTO;
import com.dolnikova.tm.util.DateUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class TaskPersistCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.PERSIST_TASK;
    }

    @NotNull
    @Override
    public String description() {
        return Command.PERSIST_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @Nullable final ProjectDTO projectDTO = findProject();
        if (projectDTO == null) return;
        System.out.println(AdditionalMessage.INSERT_TASK);
        boolean taskCreationCompleted = false;
        while (!taskCreationCompleted) {
            @NotNull final String taskText = serviceLocator.getScanner().nextLine();
            if (taskText.isEmpty()) taskCreationCompleted = true;
            else {
                @Nullable final TaskDTO newTask = new TaskDTO();
                newTask.setUserId(serviceLocator.getUserDTO().getId());
                newTask.setName(taskText);
                newTask.setProjectId(projectDTO.getId());
                System.out.println("Клиент: Вставили id проекта: " + projectDTO.getId());
                System.out.println(AdditionalMessage.INSERT_START_DATE);
                boolean dateChosen = false;
                while (!dateChosen) {
                    @NotNull String startDate = serviceLocator.getScanner().nextLine();
                    if (startDate.isEmpty()) return;
                    newTask.setDateBegin(DateUtil.stringToXMLGregorianCalendar(startDate));
                    dateChosen = true;
                }
                System.out.println(AdditionalMessage.INSERT_END_DATE);
                dateChosen = false;
                while (!dateChosen) {
                    @NotNull final String endDate = serviceLocator.getScanner().nextLine();
                    if (endDate.isEmpty()) return;
                    newTask.setDateEnd(DateUtil.stringToXMLGregorianCalendar(endDate));
                    dateChosen = true;
                }
                serviceLocator.getTaskEndpoint().persistTask(serviceLocator.getSessionDTO(), newTask);
                System.out.println(AdditionalMessage.TASK + " " + taskText + " " + AdditionalMessage.CREATED_F);
            }
        }
        System.out.println(AdditionalMessage.TASK_ADDITION_COMPLETED);
    }

    @Nullable
    private ProjectDTO findProject() {
        if (serviceLocator.getProjectEndpoint().findAllProject(serviceLocator.getSessionDTO()).isEmpty()) {
            System.out.println(AdditionalMessage.NO_PROJECTS);
            return null;
        }
        System.out.println(AdditionalMessage.CHOOSE_PROJECT);
        @Nullable ProjectDTO projectDTO = null;
        while (projectDTO == null) {
            @NotNull final String projectName = serviceLocator.getScanner().nextLine();
            if (projectName.isEmpty()) break;
            projectDTO = serviceLocator.getProjectEndpoint().findOneByNameProject(serviceLocator.getSessionDTO(), projectName);
            if (projectDTO == null) System.out.println(AdditionalMessage.PROJECT_NAME_DOESNT_EXIST + " " + AdditionalMessage.TRY_AGAIN);
        }
        return projectDTO;
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }
}
